package com.atlassian.core.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends Page {

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(how = How.ID, using = "username")
    public WebElement inputUsername;

    @FindBy(how = How.ID, using = "password")
    public WebElement inputPassword;

    @FindBy(how = How.ID, using = "login-submit")
    public WebElement buttonSignIn;

    public HomePage login(String username, String password){
        inputUsername.clear();
        inputUsername.sendKeys(username);
        inputPassword.clear();
        inputPassword.sendKeys(password);
        buttonSignIn.click();

        return PageFactory.initElements(webDriver, HomePage.class);
    }


}
