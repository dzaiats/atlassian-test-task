package com.atlassian.core.pages;

import com.atlassian.core.helper.PropertiesData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends LoginPage {

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(how = How.XPATH, using = "//*[@id='user-options']/a")
    public WebElement linkLogIn;

    @FindBy(how = How.ID, using = "create_link")
    public WebElement createNewIssueButton;

    @FindBy(how = How.ID, using = "create-issue-dialog")
    public WebElement createIssueDialog;

    @FindBy(how = How.ID, using = "summary")
    public WebElement inputSummary;

    @FindBy(how = How.ID, using = "description")
    public WebElement inputDescription;

    @FindBy(how = How.ID, using = "priority-field")
    public WebElement inputPriorityField;

    @FindBy(how = How.XPATH, using = "//a[contains(@class,'aui-list-item-link')]")
    public WebElement dropAutocompleteMenu;

    @FindBy(how = How.ID, using = "create-issue-submit")
    public WebElement buttonCreateSubmit;

    @FindBy(how = How.XPATH, using = "//div//a[contains(@href,'/browse/TST-')]")
    public WebElement popupNewIssueIsCreated;


    public LoginPage loginPage(){
        linkLogIn.click();

        return PageFactory.initElements(webDriver, LoginPage.class);
    }

    public HomePage createNewIssue(){
        createNewIssueButton.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(createIssueDialog));
        inputDescription.clear();
        inputSummary.clear();
        inputSummary.sendKeys(PropertiesData.getData("issue.summary"));
        inputDescription.sendKeys(PropertiesData.getData("issue.description"));
        selectFromAutoCompleteForm(inputPriorityField, PropertiesData.getData("issue.priority"),dropAutocompleteMenu);
        buttonCreateSubmit.click();

        return PageFactory.initElements(webDriver, HomePage.class);
    }

    public boolean isNewIssueCreated(){
        waitForLoading();
        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        wait.until(ExpectedConditions.visibilityOf(popupNewIssueIsCreated));
        return isElementPresent(popupNewIssueIsCreated);
    }



}
