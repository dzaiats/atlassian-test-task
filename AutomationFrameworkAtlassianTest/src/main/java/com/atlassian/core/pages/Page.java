package com.atlassian.core.pages;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {

	protected WebDriver webDriver;

	
	public Page(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public boolean isElementPresent(WebElement element) {
        try {
        	element.isEnabled();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
	
	public void waitForLoading(){
		JavascriptExecutor js = (JavascriptExecutor) webDriver;
		js.executeScript("$(document).ready(function(){setTimeout(function(){return true;},2000)});");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

    public void selectFromAutoCompleteForm(WebElement element, String text, WebElement dropAutocompleteMenu){
        element.clear();
        element.sendKeys(text);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(dropAutocompleteMenu));
        if (isElementPresent(dropAutocompleteMenu))
            dropAutocompleteMenu.click();
    }

	
}
