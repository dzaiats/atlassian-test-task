package com.atlassian.core.helper;

import java.io.IOException;
import java.util.Properties;

public class PropertiesData {

	private static final String TEST_FILE = "/test.properties";
	
	public static String getData(String name) {
		Properties props = new Properties();
		try {
			props.load(PropertiesData.class.getResourceAsStream(TEST_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String value = "";

		if (name != null) {
			value = props.getProperty(name);
		}
		return value;
	}
}
