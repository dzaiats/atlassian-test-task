package com.atlassiantest.tests;

import com.atlassian.core.pages.HomePage;
import com.atlassian.core.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateNewIssueSuccessfullyTest extends TestBase{

    @Test(groups="smoke", enabled=true)
    public void verifyThatNewIssueCanBeCreated(){
        LoginPage loginPage = home.loginPage();
        loginPage.login(username, password);
        HomePage homePage = home.createNewIssue();
        Assert.assertTrue(homePage.isNewIssueCreated(), "Issue is not created");

    }
}
