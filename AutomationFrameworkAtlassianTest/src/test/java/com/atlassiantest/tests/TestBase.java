package com.atlassiantest.tests;

import java.util.concurrent.TimeUnit;

import com.atlassian.core.helper.PropertiesData;
import com.atlassian.core.pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestBase {

	protected WebDriver webDriver;
	
	protected static String testUrl;
	protected static String username;
	protected static String password;
	
	protected HomePage home;

	@BeforeMethod (groups = {"smoke"})
	public void init() throws Exception {
		
		testUrl = PropertiesData.getData("base_test.url");
		username = PropertiesData.getData("login.username");
		password = PropertiesData.getData("login.password");
		
		webDriver = new FirefoxDriver();
		
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().deleteAllCookies();
		webDriver.get(testUrl);
		home = PageFactory.initElements(webDriver, HomePage.class);
	
	}
	
	@AfterMethod (groups = {"smoke"})
	public void reopenApp(){
		if (webDriver != null) {
			webDriver.quit();
		}
	}
	
	
}
