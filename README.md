Welcome to my test bitbucket repository.
====================
###Part 1
File "Part_1.txt" - is a file with questions for Product Manager

###Part 2
File "Watcher.java" - is a file with changes for 2-nd excercise. There are 3 changes which are commented

###Part 3
There are maven project "AutomationFrameworkAtlassianTest". To run this test You need to import this project as Maven project and run "testng.xml" with TestNG plugin. 
Or, if You have correctly configured Maven on Your computer, You can run with command "mvn clean install". I used Page Object here. 
But to be honest - it's not the best idea for automation. I like to mix this pattern with Abstract Factory. I like to have separatly logic for verifications,
actions etc.


P.S

There are another branch Selenium-Concordion. Automation test framework which is based on Concordion for testing Web UI Application. Very good approach for Specification by Example